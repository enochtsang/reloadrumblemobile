﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {

    private const float TriggerDeathAnimationTime_ = 0.4f;

    public int HealthContained { get; set; } = 10;
    public float Lifetime { get; set; } = float.MaxValue;

    private RRServer rrserver_;
    private bool isDying_;
    private float opacity_;
    private float lifetimeTimer_;

    void Start () {
        rrserver_ = RRServer.instance;
        isDying_ = false;
        opacity_ = 1f;
        lifetimeTimer_ = 0;
    }

    void Update() {
        if(!isDying_) {
            lifetimeTimer_ += Time.deltaTime;
            if(lifetimeTimer_ > Lifetime) {
                Destroy(gameObject);
            }
        } else {
            opacity_ -= Time.deltaTime / TriggerDeathAnimationTime_;
            var newColor = gameObject.GetComponent<SpriteRenderer>().color;
            newColor.a = opacity_;
            gameObject.GetComponent<SpriteRenderer>().color = newColor;
            gameObject.transform.localScale += new Vector3(
                Time.deltaTime / TriggerDeathAnimationTime_ / 2,
                Time.deltaTime / TriggerDeathAnimationTime_ / 2,
                Time.deltaTime / TriggerDeathAnimationTime_ / 2);
        };
    }

    public void Trigger() {
        if(isDying_) {
            return;
        }

        rrserver_.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateHealth.ToString() },
            { RRServerMsgKeys.amount, HealthContained.ToString() },
        });

        isDying_ = true;
        Destroy(this.gameObject, TriggerDeathAnimationTime_);
    }
}
