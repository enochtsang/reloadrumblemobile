﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

public class ArmorGame : MonoBehaviour {

    private const float cardsTopleftX = -1.5f;
    private const float cardsTopleftY = 1.5f;
    private const float cardSpacingX = 1f;
    private const float cardSpacingY = -1.6f;

    // cardsX * cardsY must be even
    private const int cardsX = 4;
    private const int cardsY = 4;

    private readonly string hebrewLetters = "אבגדהוזחטיךכלםמןנסעףפץצקרשתװױײ";

    public int durability = 0;
    public Text durabilityText;
    private RRServer rrserver;
    private GameObject cardPrefab;
    private List<GameObject> cards = new List<GameObject>();
    private int matches;
    private Card prevCard;

    void Start () {
        rrserver = RRServer.instance;
        rrserver.Listen(RRServerMsgType.MutateArmor, HandleMutateArmor);
        cardPrefab = Resources.Load<GameObject>("Prefabs/Defence/Card");
        DealCards();
        matches = 0;
    }

    void Update () {
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)
            || (Input.GetMouseButtonDown(0)))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if (hit != null && hit.collider != null) {
                if (hit.collider.name == "Card(Clone)") {
                    if(prevCard == null) {
                        prevCard = hit.collider.gameObject.GetComponent<Card>();
                        prevCard.Show();
                    } else {
                        var currentCard = hit.collider.gameObject.GetComponent<Card>();
                        if(prevCard.Symbol() == currentCard.Symbol() && prevCard != currentCard) {
                            matches++;
                            currentCard.Show();
                            prevCard.DisableHitBox();
                            currentCard.DisableHitBox();
                        } else {
                            prevCard.Hide();
                            currentCard.Hide();
                        }
                        prevCard = null;
                    }
                }
            }
        }

        if(matches == (cardsX * cardsY / 2)) {
            rrserver.Send(new Dictionary<string, string>() {
                { RRServerMsgKeys.type, RRServerMsgType.MutateArmor.ToString() },
                { RRServerMsgKeys.amount, (5).ToString() },
            });
            DealCards();
            matches = 0;
            prevCard = null;
        }

        durabilityText.GetComponent<Text>().text = "Durability: " + durability.ToString();
    }

    public void HandleMutateArmor(Dictionary<string, string> data) {
        int n = 0;
        if (int.TryParse(data[RRServerMsgKeys.sync], out n)) {
            durability = n;
        }
    }

    private void DealCards() {
        // clear current cards
        foreach(var card in cards) {
            Destroy(card);
        }
        cards.Clear();

        var randomLetters = RandomHebrewLetters(cardsX * cardsY / 2);

        for(int i = 0; i < cardsX; i++) {
            for(int j = 0; j < cardsY; j++) {
                cards.Add(Instantiate(
                    cardPrefab,
                    new Vector3(
                        cardsTopleftX + cardSpacingX * i,
                        cardsTopleftY + cardSpacingY * j,
                        0),
                    Quaternion.identity,
                    transform
                ));
            }
        }

        cards = cards.OrderBy( x => Random.value ).ToList();
        for(int i = 0; i < cards.Count; i++) {
            cards[i].GetComponent<Card>().SetSymbol(randomLetters[i/2]);
            cards[i].GetComponent<Card>().Hide();
        }
    }

    private string RandomHebrewLetters(int lettersNeeded) {
        // select letters
        var selectedLetters = new List<char>();
        var lettersNeededLeft = lettersNeeded;
        for(int i = 0; i < hebrewLetters.Length; i++) {
            float probablity = (float)lettersNeededLeft / (float)(hebrewLetters.Length - i);
            if(Random.value < probablity) {
                // take it
                lettersNeededLeft--;
                selectedLetters.Add(hebrewLetters[i]);
            } // else move on

            if(selectedLetters.Count == lettersNeeded) {
                // that's enough letters
                break;
            }
        }
        return new string(selectedLetters.ToArray());
    }
}
