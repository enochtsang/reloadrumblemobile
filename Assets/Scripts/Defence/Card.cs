﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {

    [SerializeField] private Text text;

    public void SetSymbol(char c) {
        text.text = c.ToString();
    }

    public string Symbol() {
        return text.text;
    }

    public void Hide() {
        GetComponent<SpriteRenderer>().color = Color.black;
    }

    public void Show() {
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    public void DisableHitBox() {
        GetComponent<BoxCollider2D>().enabled = false;
    }
}
