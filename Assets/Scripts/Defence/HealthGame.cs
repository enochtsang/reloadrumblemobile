﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using WebSocketSharp;

public class HealthGame : MonoBehaviour {
    private const float HealthSpawnFrequency_ = 1.5f;
    private const float HealthLifetime_ = 0.5f;

    private int healthRecovered_;
    private float healthTimer_;

    public Text healthRecoveredText_;

    public GameObject healthPackPrefab_;


    // Use this for initialization
    void Start () {
        healthRecovered_ = 0;
        healthTimer_ = HealthSpawnFrequency_;
    }

    // Update is called once per frame
    void Update () {
        // Handle input
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)
            || (Input.GetMouseButtonDown(0)))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if (hit != null && hit.collider != null) {
                if (hit.collider.name == healthPackPrefab_.name+"(Clone)")
                {
                    hit.collider.gameObject.GetComponent<HealthPack>().Trigger();
                    healthRecovered_ += hit.collider.gameObject.GetComponent<HealthPack>().HealthContained;
                }
            }
        }

        // Update health pack
        healthTimer_ += Time.deltaTime;
        if(healthTimer_ >= HealthSpawnFrequency_) {
            var pack = (GameObject)Instantiate(
                healthPackPrefab_,
                new Vector3(
                    Random.Range(-2f, 2f),
                    Random.Range(-2.5f, 3.5f),
                    0),
                Quaternion.identity);

            pack.transform.parent = transform;

            pack.GetComponent<HealthPack>().Lifetime = HealthLifetime_;
            healthTimer_ = Random.Range(0, HealthSpawnFrequency_ / 3);
        }

        UpdateUi();
    }

    private void UpdateUi() {
        healthRecoveredText_.GetComponent<Text>().text = "Health Recovered: " + healthRecovered_.ToString();
    }
}
