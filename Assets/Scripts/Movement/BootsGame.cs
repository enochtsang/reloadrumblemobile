﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class BootsGame : MonoBehaviour {


    private RRServer rrserver;
    private int stamina;
    private int leftValue;
    private int rightValue;

    public GameObject leftButton;
    public GameObject rightButton;
    public Text staminaText;
    public Text leftText;
    public Text rightText;

    private const int maxValue = 5;
    private const int minValue = -3;

    // Use this for initialization
    void Start () {
        rrserver = RRServer.instance;
        NextValue();
        rrserver.Listen(RRServerMsgType.MutateBoots, HandleMutateBoots);
    }

    void OnDestroy() {
        RRServer.instance.StopListening(RRServerMsgType.MutateBoots, HandleMutateBoots);
    }

    // Update is called once per frame
    void Update () {
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)
            || (Input.GetMouseButtonDown(0)))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if (hit != null && hit.collider != null) {
                if (hit.collider.name == leftButton.name) {
                    MutateStamina(leftValue);
                    NextValue();
                } else if (hit.collider.name == rightButton.name) {
                    MutateStamina(rightValue);
                    NextValue();
                }
            }
        }

        UpdateUi();
    }

    private void UpdateUi() {
        leftText.text = leftValue.ToString();
        rightText.text = rightValue.ToString();
    }

    private void MutateStamina(int n)
    {
        rrserver.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateBoots.ToString() },
            { RRServerMsgKeys.amount, n.ToString() },
        });
    }

    private void NextValue() {
        leftValue = Random.Range(minValue, maxValue);
        rightValue = Random.Range(minValue, maxValue);

        // never force player to pick less than -1
        if(leftValue < -1 && rightValue < -1) {
            if(Random.value > 0.5) { // half chance
                leftValue = -1;
            } else {
                rightValue = -1;
            }
        }
    }

    public void HandleMutateBoots(Dictionary<string, string> data) {
        float n = 0;
        if (float.TryParse(data[RRServerMsgKeys.sync], out n)) {
            staminaText.text = "Stamina: " + n.ToString("0.00");
        }
    }
}
