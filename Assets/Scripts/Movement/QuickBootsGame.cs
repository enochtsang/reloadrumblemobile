﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickBootsGame : MonoBehaviour {

    public Text[] optionTexts;
    public Button[] optionButtons;
    public Text staminaText;

    private GameObject starPrefab;
    private GameObject correctPrefab;
    private GameObject wrongPrefab;
    private Vector3 middlePosition;

    private const int minStars = 5;
    private const int maxStars = 12;
    private const float maxStarDrift = 4f;
    private const float feedbackDuration = 0.4f;

    private int numStars = 0;
    private List<GameObject> stars = new List<GameObject>();

    // Use this for initialization
    void Start () {
        starPrefab = Resources.Load<GameObject>("Prefabs/Star");
        correctPrefab = Resources.Load<GameObject>("Prefabs/Correct");
        wrongPrefab = Resources.Load<GameObject>("Prefabs/Wrong");
        middlePosition = new Vector3(0, 0, 0);
        Reset();
        for(int i = 0; i < optionButtons.Length; i++) {
            var i1 = i;
            optionButtons[i].onClick.AddListener(() => HandleOptionClicked(i1));
        }
        RRServer.instance.Listen(RRServerMsgType.MutateQuickBoots, HandleMutateQuickBoots);
    }

    void OnDestroy() {
        RRServer.instance.StopListening(RRServerMsgType.MutateQuickBoots, HandleMutateQuickBoots);
    }

    void Reset() {
        foreach(var star in stars) {
            Destroy(star);
        }

        // Create stars
        numStars = Random.Range(minStars, maxStars);
        for(int i = 0; i < numStars; i++) {
            var star = Instantiate(
                starPrefab,
                middlePosition + new Vector3(
                    (Random.value * maxStarDrift) - (maxStarDrift / 2),
                    (Random.value * maxStarDrift) - (maxStarDrift / 2),
                    0
                ),
                Quaternion.identity,
                transform
            );
            star.transform.Rotate(0f, 0f, Random.Range(0f, 360f));
            stars.Add(star);
        }

        // Set incorrect answers
        var pickedNumbers = new List<int> { numStars };
        foreach(var option in optionTexts) {
            var wrongAnswer = Random.Range(minStars, maxStars);
            while(pickedNumbers.Contains(wrongAnswer))
            {
                wrongAnswer = Random.Range(minStars, maxStars);
            }
            pickedNumbers.Add(wrongAnswer);
            option.text = wrongAnswer.ToString();
        }

        // Create correct answer
        optionTexts[Random.Range(0, optionTexts.Length)].text =
            numStars.ToString();

        Debug.Log("numStars: " + numStars);

    }

    public void HandleOptionClicked(int btnNum) {
        if(optionTexts[btnNum].text == numStars.ToString()) {
            // correct answer
            Destroy(
                Instantiate(correctPrefab, middlePosition, Quaternion.identity),
                feedbackDuration
            );
            RRServer.instance.Send(new Dictionary<string, string>() {
                { RRServerMsgKeys.type, RRServerMsgType.MutateQuickBoots.ToString() },
                { RRServerMsgKeys.amount, (3).ToString() },
            });
        } else {
            // wrong answer
            Destroy(
                Instantiate(wrongPrefab, middlePosition, Quaternion.identity),
                feedbackDuration
            );
            RRServer.instance.Send(new Dictionary<string, string>() {
                { RRServerMsgKeys.type, RRServerMsgType.MutateQuickBoots.ToString() },
                { RRServerMsgKeys.amount, (-2).ToString() },
            });
        }
        Reset();
    }

    public void HandleMutateQuickBoots(Dictionary<string, string> data) {
        float n = 0;
        if (float.TryParse(data[RRServerMsgKeys.sync], out n)) {
            staminaText.text = "Stamina: " + n.ToString("0.00");
        }
    }
}
