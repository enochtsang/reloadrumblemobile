﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ClickAndDrag : MonoBehaviour {

    private enum ClickAndDragState {
        Idle,
        Touching
    }

    private ClickAndDragState state;

    // Use this for initialization
    void Start () {
        state = ClickAndDragState.Idle;
    }

    // Update is called once per frame
    void Update () {
        switch (state) {
            case ClickAndDragState.Idle:
            IdleUpdate();
            break;
            case ClickAndDragState.Touching:
            TouchingUpdate();
            break;
        }
    }

    public bool Touching() {
        return state == ClickAndDragState.Touching;
    }

    public void InterruptTouching() {
        state = ClickAndDragState.Idle;
    }

    private void IdleUpdate() {
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)
            || (Input.GetMouseButtonDown(0)))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if (hit != null && hit.collider != null) {
                if (hit.collider.name == gameObject.name) {
                    state = ClickAndDragState.Touching;
                }
            }
        }
    }

    private void TouchingUpdate() {
        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0); // get first touch since touch count is greater than zero

            if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved || Input.GetMouseButton(0)) {
                // get the touch position from the screen touch to world point
                Vector3 touchedPos = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10));
                // lerp and set the position of the current object to that of the touch, but smoothly over time.
                transform.position = touchedPos;
            }
        } else if (Input.GetMouseButton(0)) {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            transform.position = pos;
        } else {
            state = ClickAndDragState.Idle;
        }
    }
}
