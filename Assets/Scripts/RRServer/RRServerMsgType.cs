﻿public enum RRServerMsgType {
    ServerConnectionStatusChange,
    NewWeapon,
        MutateSword,
        MutateKnife,
        MutateBoltActionRifle,
    NewDefence,
        MutateHealth,
        MutateArmor,
    NewMovement,
        MutateBoots,
        MutateQuickBoots
}
