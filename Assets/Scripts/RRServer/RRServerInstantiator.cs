﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RRServerInstantiator : MonoBehaviour {
	// Use this for initialization
	[RuntimeInitializeOnLoadMethod]
	static void InstantiateRRServer () {
		var rrserverPrefab = (GameObject)Resources.Load("RRServer", typeof(GameObject));
        DontDestroyOnLoad(Instantiate(rrserverPrefab));
	}
}
