﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class KnifeGame : MonoBehaviour {

    public int uses = 0;
    private RRServer rrserver;
    public Text usesText;
    public Button renewBladeButton;

    void Start () {
        rrserver = RRServer.instance;
        rrserver.Listen(RRServerMsgType.MutateKnife, HandleMutateKnife);
        renewBladeButton.onClick.AddListener(HandleRenewBladeButtonClicked);
    }

    void Update () {
        usesText.GetComponent<Text>().text = "Uses: " + uses.ToString();
    }

    public void HandleRenewBladeButtonClicked() {
        rrserver.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateKnife.ToString() },
            { RRServerMsgKeys.refresh, (true).ToString() },
        });
    }

    public void HandleMutateKnife(Dictionary<string, string> data) {
        int n = 0;
        if (int.TryParse(data[RRServerMsgKeys.sync], out n)) {
            uses = n;
            if(uses <= 0) {
                renewBladeButton.GetComponent<Button>().interactable = true;
            } else {
                renewBladeButton.GetComponent<Button>().interactable = false;
            }
        }
    }
}
