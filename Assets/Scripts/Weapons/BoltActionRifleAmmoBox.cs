﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltActionRifleAmmoBox : MonoBehaviour {
    public int AmmoContained { get; set; }

    private RRServer rrserver_;

    void Start () {
        AmmoContained = 1;
        rrserver_ = RRServer.instance;
    }

    void OnDestroy() {
    	Trigger();
    }

    public void Trigger() {
        rrserver_.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateBoltActionRifle.ToString() },
            { RRServerMsgKeys.amount, AmmoContained.ToString() },
        });
    }
}
