﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour {

    public bool triggered { get; set; }

    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.name == "Whetstone") {
            triggered = true;
        }
    }
}
