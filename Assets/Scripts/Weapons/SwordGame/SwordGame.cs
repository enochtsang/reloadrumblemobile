﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class SwordGame : MonoBehaviour {

    public BoundaryEnd boundaryEnd;
    public Boundaries boundaries;
    public Text sharpnessText;
    public Transform feedbackLocation;
    public int sharpness;

    private const float feedbackDuration = 0.5f;

    private RRServer rrserver;
    private GameObject correctPrefab;
    private GameObject wrongPrefab;

    // Use this for initialization
    void Start () {
        rrserver = RRServer.instance;
        rrserver.Listen(RRServerMsgType.MutateSword, HandleMutateSword);
        correctPrefab = Resources.Load<GameObject>("Prefabs/Correct");
        wrongPrefab = Resources.Load<GameObject>("Prefabs/Wrong");
    }

    // Update is called once per frame
    void Update () {
        if(boundaryEnd.triggered) {
            boundaryEnd.triggered = false;
            boundaries.triggered = false; // reset entire thing
            rrserver.Send(new Dictionary<string, string>() {
                { RRServerMsgKeys.type, RRServerMsgType.MutateSword.ToString() },
                { RRServerMsgKeys.amount, (1).ToString() },
            });
            Destroy(
                Instantiate(correctPrefab, feedbackLocation.position, Quaternion.identity),
                feedbackDuration
            );
        } else if (boundaries.triggered) {
            boundaries.triggered = false;
            rrserver.Send(new Dictionary<string, string>() {
                { RRServerMsgKeys.type, RRServerMsgType.MutateSword.ToString() },
                { RRServerMsgKeys.sync, (0).ToString() },
            });
            Destroy(
                Instantiate(wrongPrefab, feedbackLocation.position, Quaternion.identity),
                feedbackDuration
            );
            sharpness = 1;
        }

        sharpnessText.text = "Sharpness: " + sharpness.ToString();
    }

    public void HandleMutateSword(Dictionary<string, string> data) {
        int n = 0;
        if (int.TryParse(data[RRServerMsgKeys.sync], out n)) {
            sharpness = n;
        }
    }
}
