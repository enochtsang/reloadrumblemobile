﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whetstone : MonoBehaviour {

    private Vector3 homePosition;

    void Start () {
        homePosition = transform.position;
    }

    // Update is called once per frame
    void Update () {
        if(!GetComponent<ClickAndDrag>().Touching()) {
            transform.position = homePosition;
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        // transform.position = homePosition;
        GetComponent<ClickAndDrag>().InterruptTouching();
        transform.position = homePosition;
    }
}
