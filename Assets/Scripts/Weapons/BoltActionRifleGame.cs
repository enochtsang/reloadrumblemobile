﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class BoltActionRifleGame : MonoBehaviour {
    private const int boxColumnHeight = 10;
    private const int numBoxTypes = 3;
    private const int wrongBoxPenalty = 1; // amount of ammo to take away
    private const float ammoBoxSpawnChance = 0.15f; // probably from 0 - 1
    private const float boxYSpacing = 0.55f;

    private RRServer rrserver;
    private List<int> boxLocations;
    private List<GameObject> boxes;
    private int ammo;
    private GameObject boxPrefab;
    private GameObject ammoBoxPrefab;

    public Text ammoText;
    public GameObject leftButton;
    public GameObject middleButton;
    public GameObject rightButton;

    // Use this for initialization
    void Start () {
        ammo = 0;
        boxPrefab = Resources.Load<GameObject>("Prefabs/RedBox");
        ammoBoxPrefab = Resources.Load<GameObject>("Prefabs/BoltActionRifleAmmoBox");
        rrserver = RRServer.instance;
        rrserver.Listen(RRServerMsgType.MutateBoltActionRifle, HandleMutateBoltActionRifle);
        InitBoxes();
    }

    void InitBoxes(){
        boxLocations = new List<int>();
        boxes = new List<GameObject>();

        for(int i = 0; i < boxColumnHeight; i++) {
            boxLocations.Add(Random.Range(0, numBoxTypes));
        }

        int boxIndex = 0;
        foreach(int location in boxLocations)
        {
            GameObject boxToMake = boxPrefab;
            float yPos = -2.8f + boxIndex * boxYSpacing;
            float xPos = 0;
            if(location == 0) {
                xPos = -1.8f;
            } else if (location == 1) {
                xPos = 0f;
            } else { // location == 2
                xPos = 1.8f;
            }

            if(Random.value < ammoBoxSpawnChance) {
                boxToMake = ammoBoxPrefab;
            }

            var newBox = (GameObject)Instantiate(
                boxToMake,
                new Vector3(xPos, yPos, 0f),
                Quaternion.identity
            );
            newBox.transform.parent = transform;
            boxes.Add(newBox);
            boxIndex++;
        }
    }

    // Update is called once per frame
    void Update () {
        // Handle input
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)
            || (Input.GetMouseButtonDown(0)))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if (hit != null && hit.collider != null) {
                if (hit.collider.name == leftButton.name)
                {
                    if(boxLocations[0] == 0)
                    {
                        NextBox();
                    } else {
                        MutateBoltActionRifle(-wrongBoxPenalty);
                    }
                }
                else if (hit.collider.name == middleButton.name)
                {
                    if(boxLocations[0] == 1)
                    {
                        NextBox();
                    } else {
                        MutateBoltActionRifle(-wrongBoxPenalty);
                    }
                }
                else if (hit.collider.name == rightButton.name)
                {
                    if(boxLocations[0] == 2)
                    {
                        NextBox();
                    } else {
                        MutateBoltActionRifle(-wrongBoxPenalty);
                    }
                }
            }
        }

        ammoText.GetComponent<Text>().text = "Ammo: " + ammo.ToString();
    }

    void NextBox() {
        boxLocations.RemoveAt(0);
        Destroy(boxes[0]);
        boxes.RemoveAt(0);

        int newPosition = Random.Range(0, numBoxTypes);
        boxLocations.Add(newPosition);

        GameObject boxToMake = boxPrefab;
        float yPos = -2.8f + boxLocations.Count * boxYSpacing;
        float xPos = 0;

        if(newPosition == 0) {
            xPos = -1.8f;
        } else if (newPosition == 1) {
            xPos = 0f;
        } else { // newPosition == 2
            xPos = 1.8f;
        }

        if(Random.value < ammoBoxSpawnChance) {
            boxToMake = ammoBoxPrefab;
        }

        var newBox = (GameObject)Instantiate(
            boxToMake,
            new Vector3(xPos, yPos, 0f),
            Quaternion.identity
        );
        newBox.transform.parent = transform;
        boxes.Add(newBox);

        foreach(var box in boxes) {
            box.GetComponent<Transform>().Translate(0, -boxYSpacing, 0);
        }
    }

    void MutateBoltActionRifle(int n)
    {
        rrserver.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateBoltActionRifle.ToString() },
            { RRServerMsgKeys.amount, n.ToString() },
        });
    }

    public void HandleMutateBoltActionRifle(Dictionary<string, string> data) {
        int n = 0;
        if (int.TryParse(data[RRServerMsgKeys.sync], out n)) {
            ammo = n;
        }
    }
}
