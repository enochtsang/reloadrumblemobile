﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ItemManager : MonoBehaviour {

    public GameObject weaponPage;
    public GameObject defencePage;
    public GameObject movementPage;
    public GameObject noneGamePrefab;

    private GameObject weapon;
    private GameObject defence;
    private GameObject movement;
    private RRServer rrserver;

    // Use this for initialization
    void Start () {
        rrserver = RRServer.instance;
        rrserver.Listen(RRServerMsgType.NewWeapon, HandleNewWeapon);
        rrserver.Listen(RRServerMsgType.NewDefence, HandleNewDefence);
        rrserver.Listen(RRServerMsgType.NewMovement, HandleNewMovement);
        weapon = Instantiate(noneGamePrefab, weaponPage.transform);
        defence = Instantiate(noneGamePrefab, defencePage.transform);
        movement = Instantiate(noneGamePrefab, movementPage.transform);
    }

    void OnDestroy() {
        rrserver.StopListening(RRServerMsgType.NewWeapon, HandleNewWeapon);
        rrserver.StopListening(RRServerMsgType.NewDefence, HandleNewDefence);
        rrserver.StopListening(RRServerMsgType.NewMovement, HandleNewMovement);
    }

    // Update is called once per frame
    void Update () {

    }

    public void HandleNewWeapon(Dictionary<string, string> data) {
        var name = data[RRServerMsgKeys.name];
        GameObject prefab = Resources.Load<GameObject>("Prefabs/Weapon/" + name + "Game");;
        Assert.IsNotNull(prefab, "New Weapon '" + name + "' not found");

        if(prefab != null) {
            if(weapon != null) {
                Destroy(weapon.gameObject);
            }
            weapon = Instantiate(prefab, weaponPage.transform);
        }
    }
    public void HandleNewDefence(Dictionary<string, string> data) {
        var name = data[RRServerMsgKeys.name];
        GameObject prefab = Resources.Load<GameObject>("Prefabs/Defence/" + name + "Game");;
        Assert.IsNotNull(prefab, "New Defence '" + name + "' not found");

        if(prefab != null) {
            if(defence != null) {
                Destroy(defence.gameObject);
            }
            defence = Instantiate(prefab, defencePage.transform);
        }
    }
    public void HandleNewMovement(Dictionary<string, string> data) {
        var name = data[RRServerMsgKeys.name];
        GameObject prefab = Resources.Load<GameObject>("Prefabs/Movement/" + name + "Game");;
        Assert.IsNotNull(prefab, "New Movement '" + name + "' not found");

        if(prefab != null) {
            if(movement != null) {
                Destroy(movement.gameObject);
            }
            movement = Instantiate(prefab, movementPage.transform);
        }
    }
}
