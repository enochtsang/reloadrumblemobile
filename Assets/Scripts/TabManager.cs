﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class TabManager : MonoBehaviour {

    public Button[] tabs_;
    public GameObject[] pages_;

    // Use this for initialization
    void Start () {
        Assert.AreEqual(tabs_.Length, pages_.Length);

        for(int i = 0; i < tabs_.Length; i++) {
            int tempint = i; // pass by value into delegate
            tabs_[i].onClick.AddListener(delegate{handleTabClicked(tempint);});
        }

        if(tabs_.Length > 0) {
            handleTabClicked(0);
        }
    }

    // Update is called once per frame
    void Update () {

    }

    private void handleTabClicked(int tabIndex) {
         for(int i = 0; i < pages_.Length; i++) {
            if(i == tabIndex) {
                pages_[i].SetActive(true);
            } else {
                pages_[i].SetActive(false);
            }
         }
    }
}
